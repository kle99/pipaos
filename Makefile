#
#  Makefile
#

CMD="bash"

all:

docker:
	docker build -t skarbat/pipaos .

shell:
	docker run -it --privileged -v=/dev:/dev -v=/proc:/proc \
		-v "${HOME}/osimages":/var/cache/xsysroot \
		-v "${HOME}/systmp":/var/cache/systmp \
		-v $(shell pwd):/home \
		-w /home \
		skarbat/pipaos $(CMD)
